package com.pkgBean;

public class User_Registration_Bean {
	private String uname;
	private String psw;
	private String address;
	private String phonenumber;
	public String getUserName() {
		return uname;
	}
	public void setUsername(String username) {
		this.uname = username;	
	}
	public String getPassword() {
		return psw;
	}
	public void setPassword(String password) {
		this.psw = password;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhoneNumber() {
		return phonenumber;
	}
	public void setPhoneNumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}

}
