package com.pkgBean;

public class Add_Author_Bean {
	private String authorname;
	private String address;
	private String phonenumber;
	public String getAuthor_name() {
		return authorname;
	}
	public void setAuthor_name(String author_name) {
		this.authorname = author_name;	
	}
	
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhoneNumber() {
		return phonenumber;
	}
	public void setPhoneNumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}


}
