package com.pkgBean;

public class Add_Publication_Bean {
	private String publicationname;
	private String emailaddress;
	private String phonenumber;
	public String getPublication_name() {
		return publicationname;
	}
	public void setPublication_name(String publication_name) {
		this.publicationname = publication_name;	
	}
	
	public String getEmailAddress() {
		return emailaddress;
	}
	public void setEmailAddress(String emailaddress) {
		this.emailaddress = emailaddress;
	}
	public String getPhoneNumber() {
		return phonenumber;
	}
	public void setPhoneNumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}



}
