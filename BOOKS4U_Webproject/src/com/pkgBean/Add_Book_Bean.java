package com.pkgBean;

import javax.servlet.http.Part;

public class Add_Book_Bean {
	private String bookname;
	private String authorname;
	private String publicationname;
	private String price;
	private String category;
	private Part image;
	
	public String getBook_name() {
		return bookname;
	}
	public void setBook_name(String bookname) {
		this.bookname = bookname;	
	}
	public String getAuthor_name() {
		return authorname;
	}
	public void setAuthor_name(String author_name) {
		this.authorname = author_name;	
	}
	public String getPublication_name() {
		return publicationname;
	}
	public void setPublication_name(String publication_name) {
		this.publicationname = publication_name;	
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;	
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;	
	}
	public Part getImage() {
		return image;
	}
	public void setImage(Part image) {
		this.image = image;	
	}
	


}
