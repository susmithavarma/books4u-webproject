package com.pkgBean;

public class Admin_Login_Bean {
	private String uname;
	private String psw;
	public String getUserName() {
		return uname;
	}
	public void setUserName(String userName) {
		this.uname = userName;
	}
	public String getPassword() {
		return psw;
	}
	public void setPassword(String password) {
		this.psw = password;
	}
}
