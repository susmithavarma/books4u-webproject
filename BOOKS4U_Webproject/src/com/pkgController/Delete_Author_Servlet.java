package com.pkgController;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.pkgBean.Delete_Author_Bean;

import com.pkgDAO.Delete_Author_dao;

/**
 * Servlet implementation class Delete_Author_Servlet
 */
@WebServlet("/Delete_Author_Servlet")
public class Delete_Author_Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
      
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	
		String authorname=request.getParameter("author");
		System.out.println("in servlet"+authorname);
		Delete_Author_Bean authorBean = new Delete_Author_Bean();

		System.out.println(" before"+authorname);
		authorBean.setAuthor_name(authorname);
		System.out.println("after");
		Delete_Author_dao userdao = new Delete_Author_dao();
		System.out.println("123");
		String userRegistered = userdao.author(authorBean);
		System.out.println(userRegistered);
		if (userRegistered.equals("SUCCESS")) // On success, you can display a message to user on Home page
		{
			System.out.println("in if");
			//request.setAttribute("Admin_Menu", true);
			HttpSession session = request.getSession(false); 
			request.getRequestDispatcher("/Admin_Menu.jsp").forward(request, response);
		} else // On Failure, display a meaningful message to the User.
		{
			System.out.println("in else");
			
			request.setAttribute("errMessage", userRegistered);
			request.getRequestDispatcher("/Delete_Author.jsp").forward(request, response);
		}
	}
	}


