package com.pkgController;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Comics
 */
@WebServlet("/Comics")
public class Comics extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Comics() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        System.out.println("new...");
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "webproject", "webproject");
            PreparedStatement ps = con.prepareStatement("SELECT a.book_id, b.author_name,c.publication_name,a.book_name, a.price,a.image\r\n" + 
            		"from book_details a inner join author_details b on a.author_id = b.author_id\r\n" + 
            		"inner join publication_details c on a.publication_id = c.publication_id\r\n" + 
            		"inner join categories d on a.category_id = d.category_id\r\n" + 
            		"where d.category_id = 4");
            ResultSet rs = ps.executeQuery();
            while ( rs.next()) {
            	  out.print("<table width=30% border=0>");
                  out.println("<h3>" + rs.getString(4) + "</h3>");
                  out.println("<img width='200' height='200' src='Fiction_Image?bookid=" +  rs.getInt(1) + "'></img> <p/>");
          //  	  out.println("<img width='300' height='300' src='Display_Image_Servlet?bookid=21'></img> <p/>");
                 // out.print("<h3>" + rs.getInt("price") + "</h3>");
                  out.print("<tr>");

                     out.print("<td>"+"Author: "+rs.getString(2)+"</td></tr>");
                     out.print("<td>"+"Publication: "+rs.getString(3)+"</td></tr>");
                     out.print("<td>"+"Price: "+rs.getString(5)+"</td></tr>");
                     out.print("<br><a href=\"Payment.jsp\"><button type=\"button\">Buy</button></a><br>");
                    System.out.println("buy");
                     out.print("<br><a href=\"Cart_Servlet?customer_id=1&book_id="+rs.getInt(1)+"\"><button type=\"button\">Add to Cart</button></a><br>");
                     System.out.println("cart");
                     out.println("<html>");
                     out.println("<head>");
                     out.println("<title>");
                     out.println("BOOKS4U");      
                     out.println("</TITLE>");
                     out.println("</head>");
                     out.println("<body bgcolor=\"#E6E6FA\">");
          
                     out.println("</body>");
                     out.println("</html>");
                     
                  }

                  out.print("</table>");
                  out.print("<br><br><a href=\"User_Menu.jsp\">back</a><br>");
            con.close();
        }
        catch(Exception ex) {
            System.out.println(ex.getMessage());
        }
        
        finally {            
            out.close();
        }
	}

}
