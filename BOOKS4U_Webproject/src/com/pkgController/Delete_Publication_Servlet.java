package com.pkgController;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.pkgBean.Delete_Author_Bean;
import com.pkgBean.Delete_Publication_Bean;
import com.pkgDAO.Delete_Author_dao;
import com.pkgDAO.Delete_Publication_dao;

/**
 * Servlet implementation class Delete_Publication_Servlet
 */
@WebServlet("/Delete_Publication_Servlet")
public class Delete_Publication_Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Delete_Publication_Servlet() {
        super();
        // TODO Auto-generated constructor stub
    }


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String publicationname=request.getParameter("publication");
		System.out.println("in servlet"+publicationname);
		Delete_Publication_Bean publicationBean = new Delete_Publication_Bean();

		System.out.println(" before"+publicationname);
		publicationBean.setPublication_name(publicationname);
		System.out.println("after");
		Delete_Publication_dao userdao = new Delete_Publication_dao();
		System.out.println("123");
		String userRegistered = userdao.publication(publicationBean);
		System.out.println(userRegistered);
		if (userRegistered.equals("SUCCESS")) // On success, you can display a message to user on Home page
		{
			System.out.println("in if");
			HttpSession session = request.getSession(false); 
			request.getRequestDispatcher("/Admin_Menu.jsp").forward(request, response);
		} else // On Failure, display a meaningful message to the User.
		{
			System.out.println("in else");
			
			request.setAttribute("errMessage", userRegistered);
			request.getRequestDispatcher("/Delete_Publication.jsp").forward(request, response);
		}
	}

}
