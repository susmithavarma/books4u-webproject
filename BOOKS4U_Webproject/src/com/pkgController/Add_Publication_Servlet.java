package com.pkgController;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.pkgBean.Add_Publication_Bean;

import com.pkgDAO.Add_Publication_dao;

/**
 * Servlet implementation class Add_Publication_Servlet
 */
@WebServlet("/Add_Publication_Servlet")
public class Add_Publication_Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Add_Publication_Servlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out=response.getWriter();
		String publicationname=request.getParameter("publicationname");
		String emailaddress=request.getParameter("emailaddress");
		String phonenumber=request.getParameter("phonenumber");
		
		Add_Publication_Bean publicationBean = new Add_Publication_Bean();
		publicationBean.setPublication_name(publicationname);
		publicationBean.setEmailAddress(emailaddress);
		publicationBean.setPhoneNumber(phonenumber);
		

		Add_Publication_dao userdao = new Add_Publication_dao();

		String userRegistered = userdao.publication(publicationBean);
		if (userRegistered.equals("SUCCESS")) // On success, you can display a message to user on Home page
		{
			System.out.println("in if");
			
			HttpSession session = request.getSession(false); 
			request.getRequestDispatcher("/Admin_Menu.jsp").forward(request, response);
		} else // On Failure, display a meaningful message to the User.
		{
			System.out.println("in else");
			
			request.setAttribute("errMessage", userRegistered);
			request.getRequestDispatcher("/Add_Publications.jsp").forward(request, response);
		}

	}

	
	}


