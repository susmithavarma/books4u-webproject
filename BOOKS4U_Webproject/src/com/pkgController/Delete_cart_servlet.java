package com.pkgController;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.util.Database_Connection;

/**
 * Servlet implementation class Delete_cart_servlet
 */
@WebServlet("/Delete_cart_servlet")
public class Delete_cart_servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Delete_cart_servlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		
		String customer_id=request.getParameter("customer_id");
        String book_id=request.getParameter("book_id");
        System.out.println("customer_id - "+ customer_id +" book_id - "+book_id);
        Connection conn = null;
        Statement stmt=null;
        PrintWriter out=response.getWriter();
        try
        { 
        	
        	conn = Database_Connection.conn();
        	String qry="delete from cart where customer_id = ? and book_id = ?";
			PreparedStatement st = conn.prepareStatement(qry);
			st.setString(1, customer_id);
			st.setString(2, book_id);
			
			st.executeUpdate();
			System.out.println("Deleted");
			request.getRequestDispatcher("/Cart.jsp").forward(request, response);
		
		}
		catch(Exception e){
		e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
