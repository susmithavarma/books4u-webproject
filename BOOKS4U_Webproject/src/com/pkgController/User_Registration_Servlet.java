package com.pkgController;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.pkgBean.User_Registration_Bean;
import com.pkgDAO.User_Registration_dao;

/**
 * Servlet implementation class User_Registration_Servlet
 */
@WebServlet("/User_Registration_Servlet")
public class User_Registration_Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public User_Registration_Servlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out=response.getWriter();
		String username=request.getParameter("uname");
		String password=request.getParameter("psw");
		String address=request.getParameter("address");
		String phonenumber=request.getParameter("phonenumber");
		User_Registration_Bean userBean = new User_Registration_Bean();

		
		userBean.setUsername(username);
		userBean.setPassword(password);
		userBean.setAddress(address);
		userBean.setPhoneNumber(phonenumber);
		

		User_Registration_dao userdao = new User_Registration_dao();

		String userRegistered = userdao.authenticateUser(userBean);
		if (userRegistered.equals("SUCCESS")) // On success, you can display a message to user on Home page
		{
			System.out.println("in if");
			request.setAttribute("User_Login", true);

			request.getRequestDispatcher("/User_Login.jsp").forward(request, response);
		} else // On Failure, display a meaningful message to the User.
		{
			System.out.println("in else");
			request.setAttribute("playersrgstr", false);
			request.setAttribute("errMessage", userRegistered);
			request.getRequestDispatcher("/index.jsp").forward(request, response);
		}

	}

	

}
