package com.pkgController;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import com.pkgBean.Add_Book_Bean;

import com.pkgDAO.Add_Book_dao;

/**
 * Servlet implementation class Add_Book_Servlet
 */
@WebServlet("/Add_Book_Servlet")
@MultipartConfig
public class Add_Book_Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Add_Book_Servlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
        response.setContentType("text/html;charset=UTF-8");
        System.out.println("in servlet");
		PrintWriter out=response.getWriter();
		String bookname=request.getParameter("bookname");
		String authorname=request.getParameter("author");
		String publicationname=request.getParameter("publication");
		String price=request.getParameter("price");
		String category=request.getParameter("category");
		System.out.println("getting upto category");
		Part image=request.getPart("myFile");
		System.out.println("getting parameters");
		Add_Book_Bean bookBean = new Add_Book_Bean();

		
		bookBean.setBook_name(bookname);
		bookBean.setAuthor_name(authorname);
		bookBean.setPublication_name(publicationname);
		bookBean.setPrice(price);
		bookBean.setCategory(category);
		bookBean.setImage(image);
		
		

		Add_Book_dao userdao = new Add_Book_dao();

		String userRegistered = userdao.book(bookBean);
		if (userRegistered.equals("SUCCESS")) // On success, you can display a message to user on Home page
		{
			System.out.println("in if");
			//request.setAttribute("Admin_Menu", true);
			HttpSession session = request.getSession(false); 
			request.getRequestDispatcher("/Admin_Menu.jsp").forward(request, response);
		} else // On Failure, display a meaningful message to the User.
		{
			System.out.println("in else");
			
			request.setAttribute("errMessage", userRegistered);
			request.getRequestDispatcher("/add_book.jsp").forward(request, response);
		}

	}

}
