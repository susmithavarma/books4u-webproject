package com.pkgController;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.pkgBean.Admin_Login_Bean;
import com.pkgDAO.Admin_Login_dao;

/**
 * Servlet implementation class Admin_Login_servlet
 */
@WebServlet("/Admin_Login_servlet")
public class Admin_Login_servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Admin_Login_servlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
System.out.println("in post");
	PrintWriter out=response.getWriter();
		String userName = request.getParameter("uname");
		String password = request.getParameter("psw");
		System.out.println("in servlet username is " + userName +" and password" + password);
		
		Admin_Login_Bean adminBean = new Admin_Login_Bean();
		adminBean.setUserName(userName);
		adminBean.setPassword(password);
		
		Admin_Login_dao adminDao = new Admin_Login_dao();
		String userValidate = adminDao.authenticateUser(adminBean); //Calling authenticateUser function
		 
		if(userValidate.equals("SUCCESS")) //If function returns success string then user will be rooted to Home page
		 {
			HttpSession session = request.getSession(); //Creating a session
			session.setAttribute("uname", userName); //setting session attribute	
			request.getRequestDispatcher("Admin_Menu.jsp").forward(request, response);
			
			
			//request.getRequestDispatcher("/add_book.jsp").forward(request, response);//RequestDispatcher is used to send the control to the invoked page.
		 }
		 else
		 {
			 System.out.println("in servlet else");
			 request.setAttribute("Admin_Login_servlet", false);
						
		 request.setAttribute("errMessage", userValidate); //If authenticateUser() function returns other than SUCCESS string it will be sent to Login page again. Here the error message returned from function has been stored in a errMessage key.
		 request.getRequestDispatcher("/Admin_Login.jsp").forward(request, response);//forwarding the request
		 }
		
	
	}

}
