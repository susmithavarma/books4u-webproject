package com.pkgController;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.util.Database_Connection;

/**
 * Servlet implementation class Cart_Servlet
 */
@WebServlet("/Cart_Servlet")
public class Cart_Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Cart_Servlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		String customer_id=request.getParameter("customer_id");
        String book_id=request.getParameter("book_id");
        Connection conn = null;
        Statement stmt=null;
        PrintWriter out=response.getWriter();
        try
        { 
        	
        	conn = Database_Connection.conn();
        	String qry="insert into cart(cartid,customer_id,book_id) values (cartseq.nextval,?,?)";
			System.out.println("after2222");
			PreparedStatement st = conn.prepareStatement(qry);
			st.setString(1, customer_id);
			st.setString(2, book_id);
			
			st.executeUpdate();
			request.getRequestDispatcher("/User_Menu.jsp").forward(request, response);
		
		}
		catch(Exception e){
		e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
