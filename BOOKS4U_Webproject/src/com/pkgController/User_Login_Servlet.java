package com.pkgController;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.pkgBean.Admin_Login_Bean;
import com.pkgBean.User_Login_Bean;
import com.pkgDAO.Admin_Login_dao;
import com.pkgDAO.User_Login_dao;

/**
 * Servlet implementation class User_Login_Servlet
 */
@WebServlet("/User_Login_Servlet")
public class User_Login_Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public User_Login_Servlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("in post new");
		PrintWriter out=response.getWriter();
			String userName = request.getParameter("uname");
			String password = request.getParameter("psw");
			System.out.println("in servlet username is " + userName +" and password" + password);
			
			User_Login_Bean userBean = new User_Login_Bean();
			userBean.setUsername(userName);
			userBean.setPassword(password);
			
			User_Login_dao userDao = new User_Login_dao();
			String userValidate = userDao.User(userBean); //Calling authenticateUser function
			 
			if(userValidate.equals("SUCCESS")) //If function returns success string then user will be rooted to Home page
			 {
				HttpSession session = request.getSession(); //Creating a session
				session.setAttribute("uname", userName); //setting session attribute	
				request.getRequestDispatcher("User_Menu.jsp").forward(request, response);
				
				
				//request.getRequestDispatcher("/add_book.jsp").forward(request, response);//RequestDispatcher is used to send the control to the invoked page.
			 }
			 else
			 {
				 System.out.println("in servlet else");
				 request.setAttribute("Admin_Login_servlet", false);
							
			 request.setAttribute("errMessage", userValidate); //If authenticateUser() function returns other than SUCCESS string it will be sent to Login page again. Here the error message returned from function has been stored in a errMessage key.
			 request.getRequestDispatcher("/User_Login.jsp").forward(request, response);//forwarding the request
			 }
			
		
		}
	}


