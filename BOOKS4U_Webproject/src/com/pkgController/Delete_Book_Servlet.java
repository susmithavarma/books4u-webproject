package com.pkgController;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.pkgBean.Delete_Author_Bean;
import com.pkgBean.Delete_Book_Bean;
import com.pkgDAO.Delete_Author_dao;
import com.pkgDAO.Delete_Book_dao;

/**
 * Servlet implementation class Delete_Book_Servlet
 */
@WebServlet("/Delete_Book_Servlet")
public class Delete_Book_Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Delete_Book_Servlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String bookname=request.getParameter("book");
		System.out.println("in servlet"+bookname);
		Delete_Book_Bean bookBean = new Delete_Book_Bean();

		System.out.println(" before"+bookname);
		bookBean.setBook_name(bookname);
		System.out.println("after");
		Delete_Book_dao userdao = new Delete_Book_dao();
		System.out.println("123");
		String userRegistered = userdao.book(bookBean);
		System.out.println(userRegistered);
		if (userRegistered.equals("SUCCESS")) // On success, you can display a message to user on Home page
		{
			System.out.println("in if");
			//request.setAttribute("Admin_Menu", true);
			HttpSession session = request.getSession(false); 
			request.getRequestDispatcher("/Admin_Menu.jsp").forward(request, response);
		} else // On Failure, display a meaningful message to the User.
		{
			System.out.println("in else");
			
			request.setAttribute("errMessage", userRegistered);
			request.getRequestDispatcher("/Delete_Author.jsp").forward(request, response);
		}
	}

}
