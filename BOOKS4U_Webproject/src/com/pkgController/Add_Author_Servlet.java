package com.pkgController;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.pkgBean.Add_Author_Bean;
import com.pkgBean.User_Registration_Bean;
import com.pkgDAO.Add_Author_dao;
import com.pkgDAO.User_Registration_dao;

/**
 * Servlet implementation class Add_Author_Servlet
 */
@WebServlet("/Add_Author_Servlet")
public class Add_Author_Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Add_Author_Servlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out=response.getWriter();
		String authorname=request.getParameter("authorname");
		String address=request.getParameter("address");
		String phonenumber=request.getParameter("phonenumber");
		Add_Author_Bean authorBean = new Add_Author_Bean();

		
		authorBean.setAuthor_name(authorname);
		authorBean.setAddress(address);
		authorBean.setPhoneNumber(phonenumber);
		

		Add_Author_dao userdao = new Add_Author_dao();

		String userRegistered = userdao.author(authorBean);
		if (userRegistered.equals("SUCCESS")) // On success, you can display a message to user on Home page
		{
			System.out.println("in if");
			//request.setAttribute("Admin_Menu", true);
			HttpSession session = request.getSession(false); 
			
			request.getRequestDispatcher("/Admin_Menu.jsp").forward(request, response);
		} else // On Failure, display a meaningful message to the User.
		{
			System.out.println("in else");
			//request.setAttribute("playersrgstr", false);
			request.setAttribute("errMessage", userRegistered);
			request.getRequestDispatcher("/Add_Author.jsp").forward(request, response);
		}

	}

	

	}

