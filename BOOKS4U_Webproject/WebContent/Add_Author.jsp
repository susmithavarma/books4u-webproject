<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>BOOKS4U</title>
</head>
<body>
<div class="header">
<img src="logo2.png" style="width:5%">
</div>
<div class="navbar">       
		<a href="index.jsp">Home</a> 
		</div>	
<form action="Add_Author_Servlet" method=post onsubmit = "return validate()" style="border:1px solid #ccc"><br><br><br><br>
<h1 align="center">Add Author Details</h1>
  <div align="center" class="container">
    <label for="authorname"><b>Author Name</b></label><br>
    <input type="text" placeholder="" name="authorname" required><br></br>
    <label for="emailaddress"><b>Email Address</b></label><br>
    <input type="text" placeholder="enter email_address" name="address"> <br><br>
    <label for="phn_number"><b>Phone Number</b></label><br>
    <input type="text" placeholder="" name="phonenumber" required><br></br>
     
    <br><br>
    <button type="submit">ADD</button>
  </div>

  <div align="center" class="container">
    <a href="Admin_Menu.jsp">Cancel</a>
  </div>
</form>

<div class="footer">
		
		<div class="cent">
	
		 <b class="cpy">&copy; Copyright 2019 BOOKS4U.All Rights Reserved</b>
		</div>
	</div>
</body>
</html>

<style>
.header {
  overflow: hidden;
  background-color: #f1f1f1; /*background colour for header is grey*/
  padding: 10px 10px;
}
.navbar {
	overflow: hidden;
	background-color:#33CDF7;
	font-family: Arial, Helvetica, sans-serif;
}
.navbar a {
	float: left;
	font-size: 18px;
	color: black;
	text-align: center;
	padding: 16px 20px;
	text-decoration: none;
}

.navbar a:hover {
	background-color:#F38A14;
}

body {font-family: Arial, Helvetica, sans-serif;}
form {background: url('book.jpg');
background-size: 1400px 800px;
border: 3px solid #f1f1f1;}

input[type=text]{
  width: 30%;
  padding: 12px 20px;
  margin: 8px 0;
  display: inline-block;
  border: 1px solid #ccc;
  box-sizing: border-box;
}
.textarea{
width: 30%;
  padding: 12px 20px;
  margin: 8px 0;
  display: inline-block;
  border: 1px solid #ccc;
  box-sizing: border-box;
}

select,option{
	width:30%;
	padding: 12px 20px;
  margin: 8px 0;
  display: inline-block;
  border: 1px solid #ccc;
  box-sizing: border-box;
}


button {
  background-color: #4CAF50;
  color: white;
  padding: 15px 20px;
  margin: 8px 0;
  border: none;
  cursor: pointer;
  width: 10%;
}

button:hover {
  opacity: 0.8;
}

.cancelbtn {
  width: auto;
  padding: 10px 18px;
  background-color: white;
}

.container {
  padding: 30px;
}

/* Change styles cancel button and Register button on extra small screens */
@media screen and (max-width: 300px) {
  
  .a {
  border: none;
  float: left;
  font-size: 16px;
  color: black;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
}
}



.footer {
	width: 1332px;
	height: 220px;
	background:#f1f1f1;
	
}

.cpy {
	margin-left:none;
	text-align:left;
	color:black;
	font-size:20px;
}

.cent {
	margin-left: 30%;
	padding: 14px 16px;
	
}


</style>