<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>BOOKS4U</title>
<link rel="stylesheet" type="text/css" href="User_Registration.css">
</head>
<body>
<div class="header">
<img src="logo2.png" style="width:5%">
</div>
<div class="navbar">       
		<a href="index.jsp">Home</a> 
		</div>	
<form action="User_Registration_Servlet" method=post onsubmit = "return validate()" style="border:1px solid #ccc"><br><br><br><br>
<h1 align="center">Registration Form</h1>
  <div align="center" class="container">
    <label for="uname"><b>Username</b></label>
    <input type="text" placeholder="Enter Username" name="uname" required><br></br>
    <label for="psw"><b>Password</b></label>
    <input type="password" placeholder="Enter Password" name="psw" required><br></br>
    <label for="city"><b>Address</b></label>
    <textarea rows="4" cols="50" name="address"></textarea> <br><br>
    <label for="phn_number"><b>Phone Number</b></label>
    <input type="text" placeholder="" name="phonenumber" required><br></br>
     
    <button type="submit">Register</button>
  </div>

  <div align="center" class="container">
    <a href="User_Login.jsp">Cancel</a>
  </div>
</form>

<div class="footer">
		
		<div class="cent">
	
		 <b class="cpy">&copy; Copyright 2019 BOOKS4U.All Rights Reserved</b>
		</div>
	</div>
</body>
</html>