<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet"
			href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<meta charset="ISO-8859-1" name="viewport" content="width=device-width, initial-scale=1">
<title>BOOKS4U</title>
<link rel="stylesheet" type="text/css" href="index.css">
</head>
<body>

<div class="header">
<img src="logo2.png" style="width:5%">
</div>
<div class="navbar">       
		<a href="index.jsp">Home</a> 
		<a href="https://www.google.com/maps/place/Talent+Sculptors/@12.9654763,80.2120612,15z/data=!4m5!3m4!1s0x0:0x17846afb06c4d82f!8m2!3d12.9654763!4d80.2120612">About</a> 
		
		<a href="User_Login.jsp">User</a> 
		<a href="Admin_Login.jsp">Admin</a>
		
		
  
	<section> 
	<img class="mySlides" src="book background.jpg" height="450px" width="1332px">
	<img class="mySlides" src="book3.jpg" height="450px" width="1332px">
	<img class="mySlides" src="book5.jpg" height="450px" width="1332px">
	<img class="mySlides" src="book6.jpg" height="450px" width="1332px">
		</section>
	
	<div class="footer">
		
		<div class="cent">
	
		 <b class="cpy">&copy; Copyright 2019 BOOKS4U.All Rights Reserved</b>
		</div>
	</div>

</body>
</html>

<%--JAVA SCRIPT--%>

<script>     
/* Automatic Slideshow - change image every 2 seconds */
var myIndex = 0;
carousel();

function carousel() {
    var i;
    var x = document.getElementsByClassName("mySlides");
    for (i = 0; i < x.length; i++) {
       x[i].style.display = "none";
    }
    myIndex++;
    if (myIndex > x.length) {myIndex = 1}
    x[myIndex-1].style.display = "block";
    setTimeout(carousel, 4000);
}
</script>