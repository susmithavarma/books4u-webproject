<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>BOOKS4U</title>
<link rel="stylesheet" type="text/css" href="User_Login.css">
</head>
<body>

<div class="header">
<img src="logo2.png" style="width:5%">
</div>
<div class="navbar">       
		<a href="index.jsp">Home</a> 
		</div>	
<form action="User_Login_Servlet" method=post onsubmit = "return validate()" style="border:1px solid #ccc">
<h1 align="center">Login</h1>
  <div align="center" class="container">
    <label for="uname"><b>Username</b></label>
    <input type="text" placeholder="Enter Username" name="uname" required><br></br>
    <label for="psw"><b>Password</b></label>
    <input type="password" placeholder="Enter Password" name="psw" required><br></br>  
    <button type="submit">Login</button>
  </div>

  <div align="center" class="container">
    <a href="index.jsp">Cancel</a>
    <a href="User_Registration_Page.jsp">Register</a>
  </div>
</form>

<script>
function validate()
{
	var username = document.getElementById("uname").value;
	 if(username==""){
		 alert("Please fill username");	
			    	 return false;
	             }
	 var password = document.getElementById("psw").value;
		if(password ==""){
			 alert("Please fill password");	
				    	 return false;
		             }
		else{
			 if(password.length<6)
				 {
				 alert("password Should Be Minimum 5 Characters");	
		    	 return false;
				 }
			  }
}
	
	
</script>
	
		<div class="footer">
		
		<div class="cent">
	
		 <b class="cpy">&copy; Copyright 2019 BOOKS4U.All Rights Reserved</b>
		</div>
	</div>

</body>
</html>