<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import="java.sql.*"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>BOOKS4U</title>
<link rel="stylesheet" type="text/css" href="add_book.css">
</head>
<body>
<%! String driverName = "oracle.jdbc.driver.OracleDriver";%>
<%!String url = "jdbc:oracle:thin:@localhost:1521:xe";%>
<%!String user = "webproject";%>
<%!String psw = "webproject";%>
<div class="header">
<img src="logo2.png" style="width:5%">
</div>
<div class="navbar">       
		<a href="index.jsp">Home</a> 
		</div>	
<form action="Add_Book_Servlet" enctype="multipart/form-data" method=post onsubmit = "return validate()" style="border:1px solid #ccc"><br><br><br><br>
<h1 align="center">Add Book Details</h1>
  <div align="center" class="container">
    <label for="bookname"><b>Book Name</b></label><br>
    <input type="text" placeholder="" name="bookname" required><br></br>
    <label for="authorname"><b>Author Name</b></label><br>
    
    <%
	Connection con = null;
	PreparedStatement ps = null;
	try
	{
	Class.forName(driverName);
	con = DriverManager.getConnection(url,user,psw);
	String sql = "SELECT author_name,author_id FROM author_details";
	ps = con.prepareStatement(sql);
	ResultSet rs = ps.executeQuery(); 
	%>
    <select name=author>
     <option value="0">select author</option>
    <%
	while(rs.next())
	{
	String author_name = rs.getString(1); 
	String author_id = rs.getString(2);
	%>
    <option value="<%=author_id %>"><%=author_name %></option>
    <%
	}
	%>
    </select><br></br>
    <%
	}
	catch(SQLException sqe)
	{ 
	out.println(sqe);
	}
	%>
    <label for="publicationname"><b>Publication Name</b></label><br>
    <%
	Connection con1 = null;
	PreparedStatement ps1 = null;
	try
	{
	Class.forName(driverName);
	con = DriverManager.getConnection(url,user,psw);
	String sql = "SELECT publication_name,publication_id FROM publication_details";
	ps = con.prepareStatement(sql);
	ResultSet rs = ps.executeQuery(); 
	%>
    <select name=publication>
    <option value="0">select Publication</option>
    <%
	while(rs.next())
	{
	String publication_name = rs.getString(1); 
	String publication_id = rs.getString(2); 
	%>
    <option value="<%=publication_id %>"><%=publication_name %></option>
    <%
	}
	%> 
    </select><br></br>
    <%
	}
	catch(SQLException sqe)
	{ 
	out.println(sqe);
	}
	%>
    <label for="price"><b>Price</b></label><br>
    <input type="text" placeholder="" name="price" required><br></br> 
    <label for="category"><b>Category</b></label><br>
    <%
	Connection con2 = null;
	PreparedStatement ps3 = null;
	try
	{
	Class.forName(driverName);
	con = DriverManager.getConnection(url,user,psw);
	String sql = "SELECT category_name,category_id FROM categories";
	ps = con.prepareStatement(sql);
	ResultSet rs = ps.executeQuery(); 
	%>
    <select name=category>
    <option value="0">select category</option>
	<%
	while(rs.next())
	{
	String category_name = rs.getString(1); 
	String category_id = rs.getString(2); 
	%>
    <option value="<%=category_id %>"><%=category_name %></option>
    <%
	}
	%> 
	</select><br></br>
	<%
	}
	catch(SQLException sqe)
	{ 
	out.println(sqe);
	}
	%>
	<label for="image"><b>Image</b></label><br>
     <input type="file" name="myFile"><br><br>
    <button type="submit">ADD</button>
  </div>

  <div align="center" class="container">
    <a href="Admin_Menu.jsp">Cancel</a>
  </div>
</form>

<div class="footer">
		
		<div class="cent">
	
		 <b class="cpy">&copy; Copyright 2019 BOOKS4U.All Rights Reserved</b>
		</div>
	</div>
</body>
</html>