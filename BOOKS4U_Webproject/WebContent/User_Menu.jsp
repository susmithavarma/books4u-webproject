<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="java.sql.*"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>BOOKS4U</title>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<meta charset="ISO-8859-1" name="viewport"
	content="width=device-width, initial-scale=1">
<title>BOOKS4U</title>
<link rel="stylesheet" type="text/css" href="index.css">
</head>
<%! String driverName = "oracle.jdbc.driver.OracleDriver";%>
<%!String url = "jdbc:oracle:thin:@localhost:1521:xe";%>
<%!String user = "webproject";%>
<%!String psw = "webproject";%>
<body>
	<div class="header">
		<img src="logo2.png" style="width: 5%">
	</div>
	<div class="navbar">
		<a href="index.jsp">Home</a> 
		<a href="https://www.google.com/maps/place/Talent+Sculptors/@12.9654763,80.2120612,15z/data=!4m5!3m4!1s0x0:0x17846afb06c4d82f!8m2!3d12.9654763!4d80.2120612">About</a>
		<div class="dropdown">
			<button class="dropbtn">
				Categories <i class="fa fa-caret-down"></i>
			</button>
			<div class="dropdown-content">
				<a href="Fiction.jsp">Fiction</a> 
				<a href="Drama.jsp">Drama</a>
				 <a href="Horror.jsp">Horror</a> 
				 <a href="Comics.jsp">Comics</a>
				  <a href="Fantasy.jsp">Fantasy</a>
			</div>
		</div>
		<%
	Connection con = null;
	PreparedStatement ps = null;
	try
	{
	Class.forName(driverName);
	con = DriverManager.getConnection(url,user,psw);
	String sql = "select count(cartid) from cart where customer_id = 1";
	ps = con.prepareStatement(sql);
	ResultSet resultSet = ps.executeQuery(); 
    while (resultSet.next()) {
    	 %> <a href="Cart.jsp">Cart (<%=resultSet.getInt("count(cartid)")%>)</a><%
    }
	}
	catch(Exception ex){
		System.out.println(ex);
	}
	%>
		
		<a href="index.jsp">Logout</a>



		<div class="search-container">
			<form name="vinform"> 
			<input type="text" placeholder="by Book,author,Publication..." name="name" onkeyup="searchInfo()">
			<button type="submit">
				<i class="fa fa-search"></i>
			</button>
			</form> 
			<span style="position:fixed;margin-left: -14px;" id="mylocation"></span>   
		</div>
</div>
		<section>
			<img class="mySlides" src="book background.jpg" height="450px"
				width="1332px"> <img class="mySlides" src="book3.jpg"
				height="450px" width="1332px"> <img class="mySlides"
				src="book5.jpg" height="450px" width="1332px"> <img
				class="mySlides" src="book6.jpg" height="450px" width="1332px">
				
		</section>
 
		<div class="footer">

			<div class="cent">

				<b class="cpy">&copy; Copyright 2019 BOOKS4U.All Rights Reserved</b>
			</div>
		</div>
</body>
</html>
<script>
	/* Automatic Slideshow - change image every 2 seconds */
	var myIndex = 0;
	carousel();

	function carousel() {
		var i;
		var x = document.getElementsByClassName("mySlides");
		for (i = 0; i < x.length; i++) {
			x[i].style.display = "none";
		}
		myIndex++;
		if (myIndex > x.length) {
			myIndex = 1
		}
		x[myIndex - 1].style.display = "block";
		setTimeout(carousel, 4000);
	}
</script>
<script>  
var request=new XMLHttpRequest();  
function searchInfo(){  
var name=document.vinform.name.value;  
var url="Search.jsp?val="+name;  
  
try{  
request.onreadystatechange=function(){  
if(request.readyState==4){  
var val=request.responseText;  
document.getElementById('mylocation').innerHTML=val;  
}  
}//end of function  
request.open("GET",url,true);  
request.send();  
}catch(e){alert("Unable to connect to server");}  
}  
</script>