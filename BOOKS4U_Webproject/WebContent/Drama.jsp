<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import="java.sql.*"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>BOOKS4U</title>
<link rel="stylesheet" type="text/css" href="Admin_Menu.css">

<link rel="stylesheet"
			href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
<%! String driverName = "oracle.jdbc.driver.OracleDriver";%>
<%!String url = "jdbc:oracle:thin:@localhost:1521:xe";%>
<%!String user = "webproject";%>
<%!String psw = "webproject";%>
<div class="header">
<img src="logo2.png" style="width:5%">
</div>
<div class="navbar">       
	 	<a href="index.jsp">Home</a>
	 	<a href="#"><h3 style="margin:auto"> Drama </h3></a>
 	 <div class="navbar-right">
  			 <a href="User_Menu.jsp">Back</a>
    
  </div>
		
		</div>	

<form  style="border:1px solid #ccc">

<br><br>
<div class="container" >
        <table width="60%" border="1" class="table" style="margin:auto;background:ghostwhite;">
            <thead>
                <tr class="warning" >
                    <td ><h4>Author Name</h4></td>
                    <td><h4>Publication Name</h4></td>
                    <td><h4>Book Name</h4></td>
                    <td><h4>Price</h4></td>
                    <td><h4>Image</h4></td>
                    <td><h4></h4></td>
				</tr>
            </thead>  
<%
Connection con = null;
try
{
Class.forName(driverName);
con = DriverManager.getConnection(url,user,psw);
        PreparedStatement ps = con.prepareStatement("SELECT a.book_id, b.author_name,c.publication_name,a.book_name, a.price,a.image\r\n" + 
        		"from book_details a inner join author_details b on a.author_id = b.author_id\r\n" + 
        		"inner join publication_details c on a.publication_id = c.publication_id\r\n" + 
        		"inner join categories d on a.category_id = d.category_id\r\n" + 
        		"where d.category_id = 2");
        ResultSet rs = ps.executeQuery();
    while (rs.next()) {
        %> <tr class="info">
                <td><%=rs.getString("book_name")%></td>
                <td><%=rs.getString("author_name")%></td>
                <td><%=rs.getString("publication_name")%></td>
                <td><%=rs.getString("price")%></td>
                <td><img width='200' height='200' src='Fiction_Image?bookid=<%=rs.getInt("book_id")%>'></img> </td>
            	<td><button type="buynow">Buy Now</button><br><br>
            	<a href="Cart_Servlet?customer_id=1&book_id=<%=rs.getInt("book_id")%>">
            	<button type="button">Add to Cart</button></a><br></td>
            </tr>
            <%
    }
	}
	catch(Exception ex){
		System.out.println(ex);
	}
	%>
	</table>
<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>

</form>
<div class="footer">
		
		<div class="cent">
	
		 <b class="cpy">&copy; Copyright 2019 BOOKS4U.All Rights Reserved</b>
		</div>
	</div>	
	
</body>
</html>